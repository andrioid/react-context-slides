import React from "react";
import { ThemeConsumer } from "./src/theme-context";

export default ({ children }) => (
  <ThemeConsumer>
    {({ theme }) => (
      <div
        style={{
          width: "100vw",
          height: "100vh",
          backgroundColor: theme.color,
          padding: 40
        }}
      >
        {children}
      </div>
    )}
  </ThemeConsumer>
);
