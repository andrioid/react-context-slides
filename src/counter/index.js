import React from "react";
import PropTypes from "prop-types";

export default class Counter extends React.Component {
  static propTypes = {
    render: PropTypes.func.isRequired
  };

  state = {
    count: 0
  };

  increment = () => {
    this.setState(state => ({
      count: state.count + 1
    }));
  };

  render() {
    return this.props.render({
      count: this.state.count,
      increment: this.increment
    });
  }
}
