// Inspired by https://github.com/kentcdodds/simply-react/blob/master/components/first-slide.js

import React from "react";
import styled from "styled-components";
import { Image } from "mdx-deck";

const Container = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

const IconImage = styled.img`
  max-height: 70px;
  max-width: 70px;
`;

const FirstSlide = ({ title = "No title", subtitle = "" }) => (
  <div
    style={{
      position: "absolute",
      padding: 40,
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: "white",
      color: "#00bcd4",
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between"
    }}
  >
    <div
      style={{
        display: "flex",
        flex: 1,
        justifyContent: "center",
        flexDirection: "column"
      }}
    >
      <h1 style={{ margin: 5, fontSize: "5em", fontWeight: "800" }}>{title}</h1>
      <h2 style={{ margin: 5 }}>{subtitle}</h2>
    </div>
    <Footer />
  </div>
);

class Footer extends React.Component {
  render() {
    return (
      <div>
        <Container>
          <div>
            <span style={{ fontSize: "1.4em" }}>Andri Óskarsson</span>
            <br />
            <a href="https://andri.dk" style={{ color: "#046065" }}>
              www.andri.dk
            </a>
            <br />
            @andrioid
          </div>
          <div style={{ flexDirection: "column", display: "flex" }}>
            <img
              src={require("file-loader!../../img/logo.png")}
              style={{ height: 50, marginBottom: 10 }}
            />
            Betal med mobilen
          </div>
        </Container>
      </div>
    );
  }
}

export default FirstSlide;
