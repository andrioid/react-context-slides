import React from "react";

export default class CounterButton extends React.Component {
  state = {
    count: 0
  };

  handlePress = () => {
    console.log("press");
    this.setState(state => {
      return { count: state.count + 1 };
    });
  };

  render() {
    const { count } = this.state;
    const buttonTitle = count ? `Clicked ${count} times` : "Click me!";
    return <button onClick={this.handlePress}>{buttonTitle}</button>;
  }
}
