import React from "react";
import PropTypes from "prop-types";

export default class CounterButton extends React.Component {
  static propTypes = {
    count: PropTypes.number.isRequired,
    onIncrement: PropTypes.func.isRequired
  };

  render() {
    const { count } = this.props;
    const buttonTitle = count ? `Clicked ${count} times` : "Click me!";
    return <button onClick={this.props.onIncrement}>{buttonTitle}</button>;
  }
}
