import React from "react";

const themes = {
  pink: {
    color: "hotpink"
  },
  green: {
    color: "#046065"
  }
};

const ThemeContext = React.createContext();

export class ThemeProvider extends React.Component {
  state = {
    theme: "green"
  };

  setTheme = theme => {
    this.setState({ theme });
  };

  render() {
    return (
      <ThemeContext.Provider
        value={{
          theme: themes[this.state.theme],
          setTheme: this.setTheme
        }}
      >
        {this.props.children}
      </ThemeContext.Provider>
    );
  }
}

export const ThemeConsumer = ThemeContext.Consumer;
